<?php

namespace clase_sistemas\Http\Controllers;

use Illuminate\Http\Request;
use clase_sistemas\Personas;

class PersonasController extends Controller
{
    public function index()
   {
       $personas = Personas::all();
       // return view('members.index',compact('members'))
       //     ->with('i', (request()->input('page', 1) - 1) * 5);
       return $personas;
   }

   public function create()
   {
       return view('personas.create');
   }

   public function store(Request $request)
   {
       request()->validate([
           'nombre' => 'required',
           'apellido' => 'required',
           'correo' => 'required',
           'dni' => 'required',
       ]);
       Personas::create($request->all());
       return redirect()->route('personas.index')
                       ->with('success','Persona creada Satisfactoriamente');
   }

   public function show(Personas $personas)
   {
       return view('personas.show',compact('persona'));
   }

   public function edit(Member $member)
   {
       return view('personas.edit',compact('persona'));
   }

   public function update(Request $request,Personas $personas)
   {
       request()->validate([
           'nombre' => 'required',
           'apellido' => 'required',
           'correo' => 'required',
           'dni' => 'required',
       ]);
       $personas->update($request->all());
       return redirect()->route('personas.index')
                       ->with('success','Persona actualizada Satisfactoriamente');
   }

   public function destroy($id)
   {
       Personas::destroy($id);
       return redirect()->route('personas.index')
                       ->with('success','Persona Eliminada Satisfactoriamente');
   }
}
