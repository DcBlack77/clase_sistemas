<?php

namespace clase_sistemas;

use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
    protected $fillable = [
        'nombre',
        'apellido',
        'correo',
        'dni'
    ];
}
